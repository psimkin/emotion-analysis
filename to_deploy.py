from scipy import spatial
import math
import json
import numpy as np
import pandas as pd
from torch.autograd import Variable
from gensim.models import KeyedVectors
import torch.nn.functional as F
import torch.nn as nn
import torch
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--batch_size', type=int, default=10, help='size of the batches')
parser.add_argument('--channels', type=int, default=1, help='number of vector channels')
parser.add_argument('--vector_size', type=int, default=300, help='size of each image dimension')
args = parser.parse_args()

cuda = True if torch.cuda.is_available() else False
Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor

class Model(nn.Module):

    def __init__(self):
        super(Model, self).__init__()
        filter_sizes = [2, 3, 4, 5]
        num_filters = 64
        self.relu = nn.LeakyReLU(0.2, inplace=True)
        self.convs = nn.ModuleList([nn.Conv2d(args.channels, num_filters, (K, args.vector_size), bias=False) for K in filter_sizes])
        self.dropout = nn.Dropout(0.5)
        self.bn = nn.BatchNorm2d(num_features=num_filters)
        self.fc1 = nn.Linear(len(filter_sizes) * num_filters, num_filters, bias=False)
        self.bn1 = nn.BatchNorm1d(num_features=num_filters)
        self.fc2 = nn.Linear(num_filters, 3, bias=False)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        x = [self.relu(self.bn(conv(x))).squeeze(3) for conv in self.convs]
        x = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in x]
        x = torch.cat(x, 1)
        x = self.relu(self.bn1(self.fc1(x)))
        x = torch.tanh(self.fc2(x))
        return x


"""
Contractions expansion
"""
def expand_contractions(text_list, contractions_dict):
    #text_list = sentence.split(' ')
    text_expanded = []
    for text in text_list:
        text = text.rstrip()
        text = text.replace("’", "'")
        if text.lower() in contractions_dict.keys():
            expanded = contractions_dict[text.lower()]
            text_expanded.append(expanded)
        elif text in contractions_dict.keys():
            expanded = contractions_dict[text]
            text_expanded.append(expanded)
        else:
            text_expanded.append(text)
    return ' '.join(text_expanded)


"""
Text cleaning for word-level encoding
"""
def clean_words(texts):
    # load contraction dictionary
    with open('contractions.json', encoding='utf-8', errors='ignore') as json_file:
        contraction_dict = json.load(json_file)
    data = []
    max_length = 0
    words = ['the', 'a', 'an', 'and', 'as', 'at', 'by', 'then', 'so']
    chars = ['"', ':', ',', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '(', ')', "'", ']', '[', ' ', '/', '-', ';']

    for text in texts:
        #print(text)
        # split punctuation
        text_split_punctuation = []
        for each in text.split(' '):
            if each in words:
                continue
            if each.isalpha():
                text_split_punctuation.append(each)
            else:
                if ('@' in each) or ('#' in each):
                    continue
                chars_str = ''
                chars_lst = []
                for char in each:
                    if char.isalpha() or char == "'" or char == "’":
                        chars_str += char
                    elif char in chars:
                        continue
                    else:
                        #chars_lst.append(chars_str)
                        chars_lst.append(char)
                        #chars_str = ''
                if chars_str is not '':
                    text_split_punctuation.append(chars_str)
                text_split_punctuation.extend(chars_lst)
        # now we have tokenized list with separate punctuation
        # replace contractions
        text_expanded = expand_contractions(text_split_punctuation, contraction_dict)

        if len(text_expanded.split(' ')) > max_length:
            max_length = len(text_expanded.split(' '))
        data.append(text_expanded)
    return data, max_length

"""
Save vector matrix for data
"""
def encode_words(texts, model, dim):
    data, rows = clean_words(texts)
    #print(rows)
    # make vectors dependant on model
    word_matrix = np.zeros((len(texts), 1, rows, dim))
    for i, sentence in enumerate(data):
        words = np.zeros((rows, dim))
        for j, word in enumerate(sentence.split(' ')):
            try:
                """
                use word2vec model here
                """
                words[j, :] = model[word]
            except Exception as e:
                pass
        word_matrix[i, 0, :, :] = words
    # return vectors as np array
    np_words = np.asarray(word_matrix)
    return np_words


if __name__ == "__main__":

    """
    load CSV file into file variable
    """
    #file =
    file = 'sentiment_dataset.csv'
    df = pd.read_csv(file)

    text_column_name = 'text'
    dat_file_name = 'vectors.dat'
    """
    load word2vec mode here into variable embed_model
    """
    #embed_model =
    embed_model = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin.gz', binary=True)

    dim = 300
    #print('Model loaded.')
    corpus = df[text_column_name].astype(str)
    encoded = encode_words(corpus.tolist(), embed_model, dim)
    #encoded.dump(dat_file_name)
    text_tensor = torch.FloatTensor(encoded)
    mini = torch.min(text_tensor)
    maxi = torch.max(text_tensor)
    #print(mini)
    #print(maxi)
    #print(text.shape)
    text_tensor_norm = -1 + ((text_tensor - mini) * 2) / (maxi - mini)

    model = Model()
    # load models
    model.load_state_dict(torch.load('model.pth'))
    no_batches = math.ceil(len(text_tensor_norm)/args.batch_size)
    all_pad = np.zeros((len(text_tensor_norm), 3))

    i = 0
    for batch in range(no_batches):

        text_batch = text_tensor_norm[batch * args.batch_size: (batch + 1) * args.batch_size, :, :, :]
        text = Variable(text_batch.type(Tensor), requires_grad=False)
        output = model(text)
        all_pad[batch*args.batch_size:(batch+1)*args.batch_size, :] = output.detach().numpy()

    mini_scale1 = 2.4
    maxi_scale1 = 9.2
    mini_scale2 = 1.57
    maxi_scale2 = 8.72
    #print(all_pad[0:5])
    all_pad_norm1 = mini_scale1 + ((all_pad + 1) * (maxi_scale1 - mini_scale1)) / 2
    #print(all_pad_norm1[0:5])
    all_pad_norm2 = mini_scale2 + ((all_pad_norm1 - mini_scale1) * (maxi_scale2 - mini_scale2)) / (maxi_scale1 - mini_scale1)
    #print(all_pad_norm2[0:5])

    df['p'] = all_pad_norm2[:, 0]
    df['a'] = all_pad_norm2[:, 1]
    df['d'] = all_pad_norm2[:, 2]

    df_emotions = pd.read_csv('PAD_emotions_deploy.csv')
    emotions_lst = df_emotions['emotion'].tolist()
    p = df_emotions['p'].tolist()
    a = df_emotions['a'].tolist()
    d = df_emotions['d'].tolist()
    pad = [p, a, d]
    emotions = [[row[i] for row in pad] for i in range(len(p))]
    #print(np.min(emotions))
    #print(np.max(emotions))
    #print(emotions[0:10])
    np_emotions = np.asarray(emotions)

    tree = spatial.KDTree(np_emotions)
    emotions_out = []
    key_emotions = []
    sentiment = []
    for each in all_pad_norm2:
        emotion_out = tree.query(each, k=5)
        #print(emotion_out[1])
        emotions_tmp = []
        for em in emotion_out[1]:
            emotions_tmp.append(emotions_lst[em])
        emotions_out.append(emotions_tmp)
        key_emotions.append(df_emotions[df_emotions['emotion'] == emotions_tmp[0]]['group'].tolist())
        sentiment.append(df_emotions[df_emotions['emotion'] == emotions_tmp[0]]['sentiment'].tolist())

    df['emotions'] = emotions_out
    df['key emotion'] = key_emotions
    df['sentiment'] = sentiment

    df.to_csv('emotions.csv')
